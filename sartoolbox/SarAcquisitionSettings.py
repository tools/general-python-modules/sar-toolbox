import numpy as np
from radarTools import AcquisitionSettings
import logging

class SarAcquisitionSettings(AcquisitionSettings):
    """
    Acquisition settings for a SAR system

    This class extends radarTools.AcqusitionSettings to add information such
    as platform locations, platform velocity etc.
    """

    def __init__(self,
                 antennaBeamwidth=180.0,
                 trajectoryCenter=None,
                 stepSize=None,
                 Nx=None,
                 startStop=True,
                 platformVelocity=None,
                 **kwargs):
        """ Initialize SAR acquisition settings

        Parameters
        ----------
        antennaBeamwidth : double
            antenna beamwidth in degrees, used to determine
            minimum azimuth sample rate (typically HPBW or FNBW)

        trajectoryCenter : np.ndarray
            3-element array giving the x, y, and z coordinates of the
            trajectory center location

        Nx : int
            number of azimuth samples (range profiles) to collect

        stepSize : double
            the distance between azimuth samples in m

        startStop : bool
            if true, the SAR platform stops at every measurement location to
            record a range profile
            (default: True)

        platformVelocity : double, optional
            the nominal velocity of the platform in azimuth direction
            (default: None)
            if None is specified, it will be computed from the chirp duration
            and minimum azimuth sample frequency

        For remaining **kwargs refer to radarTools.AcquisitionSettings
        """
        super().__init__(**kwargs)

        self.logger = logging.getLogger(__name__)
        self.startStop = startStop

        self.antennaBeamwidth = antennaBeamwidth
        self.trajectoryCenter = trajectoryCenter
        self.stepSize = stepSize
        self.Nx = Nx
        self.startStop = startStop
        self.platformVelocity = platformVelocity

        if self.startStop:
            # maximum Doppler frequency in 1/m
            fdMax = 2*np.sin(np.deg2rad(antennaBeamwidth/2))/self.lambdaMin
            self.azimuthSampleRateMin = 2*fdMax

            if stepSize is not None:
                self.azimuthSampleRate = 1/stepSize
            else:
                self.azimuthSampleRate = self.azimuthSampleRateMin

            self.pulseRepetitionRate = 1/self.T
            # platform is stationary during the measurement, however it is
            # still possible to define an effective velocity as 
            # v = delta_x / delta_t
            # where
            # delta_x = 1/azimuthSampleRateMin
            # delta_t = chirp duration
            self.platformVelocity = self.pulseRepetitionRate \
                                  / self.azimuthSampleRate

        else:
            if platformVelocity is not None:
                self.platformVelocity = platformVelocity
            else:
                raise ValueError("For continuous platform movement \
                                  a platformVelocity must be specified")

            # maximum Doppler frequency in 1/m
            fdMax = 2*np.sin(np.deg2rad(antennaBeamwidth/2)) \
                    / self.lambdaMin

            self.azimuthSampleRateMin = 2*fdMax

            if self.trigRate is not None:
                self.pulseRepetitionRate = self.trigRate
                self.azimuthSampleRate = self.pulseRepetitionRate \
                                       / self.platformVelocity
            else:
                raise ValueError("For continuous platform movement a \
                                  trigRate must be specified")

        self.trajectoryCenter = trajectoryCenter
        # number of azimuth samples
        self.Nx = Nx
        # number of real-valued range samples
        self.Nr_real = self.M

    @property
    def azimuthSampleRate(self):
        return self._azimuthSampleRate

    @azimuthSampleRate.setter
    def azimuthSampleRate(self, new):
        if new < self.azimuthSampleRateMin:
            raise ValueError(f"Azimuth sample rate {new}/m too low for chosen \
                             system parameters, minimum azimuth sample rate is\
                             {self.azimuthSampleRateMin}/m")
        else:
            self._azimuthSampleRate = new

    @property
    def trajectoryCenter(self):
        return self._trajectoryCenter

    @trajectoryCenter.setter
    def trajectoryCenter(self, new):
        if isinstance(new, np.ndarray) and new.size == 3:
            self._trajectoryCenter = new
        else:
            raise ValueError("trajectoryCenter must be 3-element np.ndarray")


    def platformLocations(self):
        """ Compute the platform locations during measurement for the given
        acquisition settings
        """
        # sample spacing in azimuth direction
        dx  = 1/self.azimuthSampleRate

        # fast time variable
        t = np.linspace(0, self.T, self.Nr_real)

        locations = np.zeros((self.Nx, self.Nr_real, 3)) 
        # y and z coordinates are constant
        locations[:,:,1:] = self.trajectoryCenter[1:]

        # x coordinate varies between measurements
        locations[:,:,0] = dx * np.linspace(-self.Nx/2,
                                            self.Nx/2,
                                            self.Nx)[:,np.newaxis]

        if not self.startStop:
            # the radar platform moves during measurement, x coordinate varies
            # linearly with fast time during the measurement
            locations[:,:,0] += self.platformVelocity * t[np.newaxis,:]

        return locations
