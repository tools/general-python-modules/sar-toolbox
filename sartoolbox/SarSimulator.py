import numpy as np
from scipy import linalg

class SarSimulator():
    """
    Generate SAR baseband data for simulation purposes
    """

    def __init__(self, acqSettings):
        """
        Initialize SAR simulator

        Paramters
        ---------
        acqSettings : SarAcquisitionSettings object
            Object that holds the acquisition settings
        """
        self.acqSettings = acqSettings

    def generate_real_baseband(self, scatterers, platform_locations=None):
        """
        Generate real-valued baseband data as produced by a
        coherent dechirp-on-receive FMCW radar sensor

        Parameters
        ----------
        scatterers : list of Scatterer
            The scatterers in the imaged scene

        platform_locations : np.ndarray, optional
            compute the baseband signal as recorded from these platform
            locations, instead of the nominal locations
            array size should be Nx * 3, i.e. one 3-element vector per nominal
            azimuth location

        Returns
        -------
        baseband : np.ndarray
            the real-valued baseband signal
        """
        if platform_locations is None:
            platform_locations = self.acqSettings.platformLocations()

        # fast time variable
        t = np.linspace(0, self.acqSettings.T, self.acqSettings.Nr_real)

        baseband = np.zeros((self.acqSettings.Nx, self.acqSettings.Nr_real))

        for scat in scatterers:
            # line of sight distance (i.e. range) from the radar platform to
            # the scatterer for every platform location
            r = linalg.norm(
                    scat.location - platform_locations,
                    axis=2)
            # propagation delay
            td = 2*r/self.acqSettings.c
            # resulting signal phase
            # desired phase variation
            phi = -2 * np.pi \
                * (self.acqSettings.fc + self.acqSettings.s*t)[np.newaxis,:] \
                * td
            # residual video phase from dechirp on receive approach
            phi += np.pi * self.acqSettings.s * td**2
            # add signal history from this scatterer to total
            baseband += scat.amplitude * np.cos(phi)

        return baseband


class Scatterer:
    def __init__(self, amplitude, location):
        """
        A scatterer in the scene

        Parameters
        ----------
        amplitude : double
            The reflected signal amplitude of this scatterer

        location : np.ndarray
            A 3-element array containing the x, y, z position of this scatterer
        """
        self.amplitude = amplitude
        self.location = location
