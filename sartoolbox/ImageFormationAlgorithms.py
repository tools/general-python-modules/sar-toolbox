import numpy as np
from scipy import signal,linalg,interpolate
import logging

from .SarImage import SarImage

class RangeMigrationAlgorithm():
    """
    Compute an image from the deskewed complex baseband signal using the
    wavenumber domain algorithm, also known as the range migration
    algorithm (RMA)

    This implementation is based on [1] and does not take into account
    platform movement during range profile collection.

    References
    ----------
    [1] W. G. Carrara, R. S. Goodman, and R. M. Majewski, "Spotlight
        synthetic signal aperture radar signal processing algorithms",
        Boston, MA, Artech House, 1995
    """
    def __init__(self, acqSettings, procSettings):
        """
        Initialize SAR image formation algorithm

        Parameters
        ----------
        acqSettings : radarTools.AcquisitionSettings object
            Object that holds the acquisition settings

        procSettings : SarProcessingSettings object
            Object that holds the processing settings
        """
        self.acqSettings = acqSettings
        self.procSettings = procSettings
        self.logger = logging.getLogger(__name__)

        # Initialize variables common to all SAR processing algorithms
        # azimuth axis for np.ndarray
        self.azimuth_axis = 0
        # range axis for np.ndarray
        self.range_axis = 1
        # reference range = shortest range to scene center
        self.reference_range = linalg.norm(self.acqSettings.trajectoryCenter[1:])

    def compute_image(self, baseband, measured_locations=None):
        """
        Compute an image using the selected algorithm

        Parameters
        ----------
        baseband : np.ndarray
            the baseband signal

        measured_locations : np.ndarray or None
            (Nx,3) array of measured location vectors of the radar platform
            for every range profile in baseband, used for motion compensation

        Returns
        -------
        image : SarImage object
            a complex valued SAR image
        """
        # compute the needed variables
        self._compute_variables(baseband.shape)

        if measured_locations is not None:
            # perform bulk motion compensation
            baseband *= self._bulk_moco_function(measured_locations)

            # interpolate in azimuth to fix non-uniform sampling
            if self.procSettings.resample_azimuth:
                baseband = self._resample_azimuth(baseband, measured_locations)

        # zero pad the signal if requested
        if self.procSettings.zero_padding is not None:
            (Nx,Nr) = baseband.shape
            # how many samples to add before and after signal in azimuth
            self.numpad = int(np.ceil(self.procSettings.zero_padding - Nx)/2)
            # zero pad the baseband signal in azimuth direction
            padded_baseband = np.pad(baseband,
                                     ((self.numpad,self.numpad),(0,0)),
                                     mode='constant',
                                     constant_values=(np.complex(0,0),))

            baseband = padded_baseband

            # update algorithm variables based on new baseband shape after
            # zero padding
            self._compute_variables(baseband.shape)
        else:
            self.numpad = 0

        # compute a radar image
        image = self._algorithm(baseband, measured_locations)

        return SarImage(image, self.crossrange, self.downrange)

    def _bulk_moco_function(self, measured_locations):
        """
        Compute the phase compensation function for bulk motion compensation
        also reffered to as motion compensation to a line

        Parameters
        ----------
        measured_locations : np.ndarray
            (Nx,3) array of measured location vectors of the radar platform
            for every range profile in baseband, used for motion compensation

        Returns
        -------
        np.ndarray
            The motion compensation function
        """
        self.drref = linalg.norm(measured_locations[:,1:], axis=1) \
                   - self.reference_range
        return np.exp(1j*self.Kr[np.newaxis,:]*self.drref[:,np.newaxis])

    def _resample_azimuth(self, baseband, measured_locations):
        """
        Interpolate the baseband samples to correct for non-uniform
        sampling in azimuth direction

        Parameters
        ----------
        baseband : np.ndarray
            The baseband data

        measured_locations : np.ndarray
            (Nx,3) array of measured location vectors of the radar platform
            for every range profile in baseband, used for motion compensation

        Returns
        -------
        new : np.ndarray
            The interpolated baseband data
        """
        nominal_locations = self.acqSettings.platformLocations()
        new = np.empty_like(baseband)
        for j in range(baseband.shape[self.range_axis]):
            new[:,j] = np.interp(nominal_locations[:,0,0],
                                 measured_locations[:,0],
                                 baseband[:,j],
                                 left=np.complex(0.0,0.0),
                                 right=np.complex(0.0,0.0))

        return new

    def _algorithm(self, baseband, measured_locations):
        """
        Parameters
        ----------
        baseband : numpy.ndarray
            complex baseband signal which has been deskewed

        measured_locations : None
            dummy parameter

        Returns
        -------
        image : SarImage object
            the SAR image
        """
        # array for Stolt interpolated data
        stolt = np.empty_like(baseband)

        # transform data into spatial frequency domain
        spatialfreq = np.fft.fftshift(
                        np.fft.fft(baseband, axis=self.azimuth_axis),
                        axes=self.azimuth_axis)

        # Reference function multiplication
        spatialfreq *= self.reference_function

        # Stolt interpolation
        # Resample signal history which was originally sampled on a uniform
        # Kx-Kr grid to a uniformly sampled Kx-Ky grid
        for i in range(self.Kx.size):
            stolt[i,:] = np.interp(
                            self.Ky_unif,
                            self.Ky[i,:],
                            spatialfreq[i,:],
                            left=np.complex(0.0, 0.0),
                            right=np.complex(0.0, 0.0))

        # apply aperture weighting in range direction
        stolt *= self.range_window[np.newaxis,:]
        # apply aperture weighting in azimuth direction
        stolt *= self.azimuth_window[:,np.newaxis]

        # final image compression
        if self.reference_range == 0:
            image = np.fft.ifft2(stolt)
        else:
            image = np.fft.fftshift(np.fft.ifft2(stolt), axes=self.range_axis)

        return image


    def _compute_variables(self, shape):
        """
        Compute variables needed for the RMA based on the baseband shape

        Parameters
        ----------
        shape : (int, int)
            the shape of the baseband data array

        Sets
        -------
        Kx : np.ndarray
            wavenumber in azimuth direction

        Kr : np.ndarray
            wavenumber in range direction

        Ky : np.ndarray
            non-uniformly spaced Kx-Ky grid

        Ky_unif : np.ndarray
            uniformly spaced Ky sample points used for stolt interpolation

        crossrange : np.ndarray
            image pixel locations in azimuth direction

        downrange : np.ndarray
            image pixel locations in range direction
        """
        Nx,Nr = shape

        # fast time variable
        self.t = np.linspace(0, self.acqSettings.T, Nr)

        # wavenumber in azimuth (x) direction
        self.Kx = np.pi * np.linspace(-1, 1, Nx) \
                * self.acqSettings.azimuthSampleRate

        # wavenumber in range direction
        self.Kr = 4 * np.pi / self.acqSettings.c \
           * (self.acqSettings.fc + self.acqSettings.s * self.t)

        # wavenumber in y direction, non-uniformly spaced
        Ky = self.Kr[np.newaxis,:]**2 - self.Kx[:,np.newaxis]**2
        # mask invalid values of Ky
        Ky[Ky < 0.0] = 0.0
        self.Ky = np.sqrt(Ky)

        # Wavenumber in y direction, uniformly spaced
        self.Ky_unif = np.linspace(np.min(self.Ky), np.max(self.Ky), self.Kr.size)

        # crossrange pixel locations in m
        self.crossrange = np.linspace(-int(Nx/2), int(Nx/2)-1, Nx) \
                        / self.acqSettings.azimuthSampleRate

        # downrange pixel locations in m
        if self.reference_range == 0:
            self.downrange = 2 * np.pi * np.arange(Nr) \
                           / (np.max(self.Ky) - np.min(self.Ky))
        else:
            self.downrange = np.linspace(-Nr/2, int(Nr/2)-1, Nr) \
                           * 2 * np.pi \
                           / (np.max(self.Ky) - np.min(self.Ky))

        # compute new reference function
        self.reference_function = self._reference_function()

        self.azimuth_window = self._azimuth_window(Nx)
        self.range_window = self._range_window(Nr)

    def _reference_function(self):
        """
        Reference function used in RMA
        """
        return np.exp(1j*self.reference_range*self.Ky)

    def _azimuth_window(self,N):
        """
        Return a window of length N for aperture weighting in the azimuth
        direction

        Paramters
        ---------
        N : int
            the window length

        Returns
        -------
        np.ndarray
            samples of the window
        """
        return self._window(self.procSettings.azimuth_window_type)(N)

    def _range_window(self,N):
        """
        Return a window of length N for aperture weighting in the range
        direction

        Paramters
        ---------
        N : int
            the window length

        Returns
        -------
        np.ndarray
            samples of the window
        """
        return self._window(self.procSettings.range_window_type)(N)

    def _window(self, window_type):
        """
        Return a window of the specified type
        """
        if window_type == "Rectangular":
            return signal.windows.boxcar
        elif window_type == "Hann":
            return signal.windows.hann
        elif window_type == "Nuttall":
            return signal.windows.nuttall
        elif window_type == "Taylor":
            return lambda n: signal.windows.taylor(n, 5, 35)

class RangeMigrationAlgorithmMoco(RangeMigrationAlgorithm):

    def __init__(self, acqSettings, procSettings):
        super().__init__(acqSettings, procSettings)

    def _algorithm(self, baseband, measured_locations):
        """
        Parameters
        ----------
        baseband : numpy.ndarray
            complex baseband signal which has been deskewed

        measured_locations : np.ndarray
            (Nx,3) array of measured location vectors of the radar platform
            for every range profile in baseband, used for motion compensation

        Returns
        -------
        image : SarImage object
            the SAR image
        """
        # array for Stolt interpolated data
        stolt = np.empty_like(baseband)

        # transform data into spatial frequency domain
        spatialfreq = np.fft.fftshift(
                        np.fft.fft(baseband, axis=self.azimuth_axis),
                        axes=self.azimuth_axis)

        # Reference function multiplication
        spatialfreq *= self.reference_function

        # Stolt interpolation
        # Resample signal history which was originally sampled on a uniform
        # Kx-Kr grid to a uniformly sampled Kx-Ky grid
        for i in range(self.Kx.size):
            stolt[i,:] = np.interp(
                            self.Ky_unif,
                            self.Ky[i,:],
                            spatialfreq[i,:],
                            left=np.complex(0.0, 0.0),
                            right=np.complex(0.0, 0.0))

        # apply aperture weighting in range direction
        stolt *= self.range_window[np.newaxis,:]
        # apply aperture weighting in azimuth direction
        stolt *= self.azimuth_window[:,np.newaxis]

        # image compression
        if self.reference_range == 0:
            image = np.fft.ifft2(stolt)
        else:
            image = np.fft.fftshift(np.fft.ifft2(stolt), axes=self.range_axis)

        # range dependent range error
        dr = np.sqrt((self.y_nominal[np.newaxis,:] - measured_locations[:,1][:,np.newaxis])**2 + measured_locations[:,2][:,np.newaxis]**2) \
           - self.r_nominal[np.newaxis,:]
        # second step of moco
        # remove the range variant azimuth modulation error
        if self.numpad > 0:
            image[self.numpad:-self.numpad,:] *= np.exp(1j*self.Kr[0]*(dr - self.drref[:,np.newaxis]))
        else:
            image *= np.exp(1j*self.Kr[0]*(dr - self.drref[:,np.newaxis]))
        # azimuth FT
        stolt = np.fft.fft(image, axis=self.azimuth_axis)
        # azimuth matched filtering
        stolt *= np.exp(1j*self.r_nominal[np.newaxis,:]*self.Ky0[:,np.newaxis])
        # azimuth IFT
        image = np.fft.ifft(stolt, axis=self.azimuth_axis)

        image[np.isnan(image)] = np.complex(0.0, 0.0)
        image[np.isinf(image)] = np.complex(0.0, 0.0)

        return image

    def _compute_variables(self, shape):
        """
        Compute variables needed for the RMA based on the baseband shape

        Parameters
        ----------
        shape : (int, int)
            the shape of the baseband data array

        Sets
        -------
        Kx : np.ndarray
            wavenumber in azimuth direction

        Kr : np.ndarray
            wavenumber in range direction

        Ky : np.ndarray
            non-uniformly spaced Kx-Ky grid

        Ky_unif : np.ndarray
            uniformly spaced Ky sample points used for stolt interpolation

        crossrange : np.ndarray
            image pixel locations in azimuth direction

        downrange : np.ndarray
            image pixel locations in range direction
        """
        Nx,Nr = shape

        # fast time variable
        self.t = np.linspace(0, self.acqSettings.T, Nr)

        # wavenumber in azimuth (x) direction
        self.Kx = np.pi * np.linspace(-1, 1, Nx) \
                * self.acqSettings.azimuthSampleRate

        # wavenumber in range direction
        self.Kr = 4 * np.pi / self.acqSettings.c \
           * (self.acqSettings.fc + self.acqSettings.s * self.t)

        # wavenumber in y direction, non-uniformly spaced
        Ky = self.Kr[np.newaxis,:]**2 - self.Kx[:,np.newaxis]**2
        Ky0 = self.Kr[0]**2 - self.Kx**2
        # mask invalid values of Ky
        Ky[Ky < 0.0] = 0.0
        Ky0[Ky0 < 0.0] = 0.0

        self.Ky0 = np.sqrt(Ky0)
        self.Ky = np.sqrt(Ky) - self.Ky0[:,np.newaxis]

        # Wavenumber in y direction, uniformly spaced
        self.Ky_unif = np.linspace(np.min(self.Ky), np.max(self.Ky), self.Kr.size)

        # crossrange pixel locations in m
        self.crossrange = np.linspace(-int(Nx/2), int(Nx/2)-1, Nx) \
                        / self.acqSettings.azimuthSampleRate

        # downrange pixel locations in m
        if self.reference_range == 0:
            self.downrange = 2 * np.pi * np.arange(Nr) \
                           / (np.max(self.Ky) - np.min(self.Ky))
        else:
            self.downrange = np.linspace(-Nr/2, int(Nr/2)-1, Nr) \
                           * 2 * np.pi \
                           / (np.max(self.Ky) - np.min(self.Ky))

        # compute new reference function
        self.reference_function = self._reference_function()

        # nominal range to target
        self.r_nominal = self.reference_range + self.downrange
        # projection of the nominal range onto the y-Plane at z = 0
        self.y_nominal = self.acqSettings.trajectoryCenter[1] - np.sqrt(self.r_nominal**2 - self.acqSettings.trajectoryCenter[2]**2)

        self.azimuth_window = self._azimuth_window(Nx)
        self.range_window = self._range_window(Nr)

    def _reference_function(self):
        """
        Modified reference function used in RMA
        """
        return np.exp(1j*self.reference_range*self.Ky)

class RangeMigrationAlgorithmFMCW(RangeMigrationAlgorithm):
    """
    Compute an image from the deskewed complex baseband signal using the
    wavenumber domain algorithm, also known as the range migration
    algorithm (RMA)

    This implementation is based on [1] but also takes into account platform
    movement during range profile collection as described in [2].

    References
    ----------
    [1] W. G. Carrara, R. S. Goodman, and R. M. Majewski, "Spotlight
        synthetic signal aperture radar signal processing algorithms",
        Boston, MA, Artech House, 1995

    [2] S. Guo and X. Dong, "Modified Omega-K algorithm for ground-based FMCW
        SAR imaging," 2016 IEEE 13th International Conference on Signal
        Processing (ICSP), Chengdu, 2016, pp. 1647-1650,
        doi: 10.1109/ICSP.2016.7878107.
    """
    def __init__(self, acqSettings, procSettings):
        super().__init__(acqSettings, procSettings)

    def _reference_function(self):
        """
        Modified reference function used in RMA
        """
        phi = self.reference_range * self.Ky \
            - self.Kx[:,np.newaxis] * self.acqSettings.platformVelocity \
            * self.t[np.newaxis,:]
        return np.exp(1j*phi)


class RangeMigrationAlgorithmBistatic(RangeMigrationAlgorithm):
    """
    Compute an image from the deskewed complex baseband signal using the
    wavenumber domain algorithm, also known as the range migration
    algorithm (RMA), taking into acccount the possibly bistatic nature
    of the signal collection geometry at high platform velocities.

    This implementation is based on [1].

    References
    ----------
    [1] R. Wang, O. Loffeld, H. Nies, S. Knedlik, M. Hagelen and H. Essen,
        "Focus FMCW SAR Data Using the Wavenumber Domain Algorithm," in IEEE
        Transactions on Geoscience and Remote Sensing, vol. 48, no. 4,
        pp. 2109-2118, April 2010, doi: 10.1109/TGRS.2009.2034368.
    """
    def __init__(self, acqSettings, procSettings):
        super().__init__(acqSettings, procSettings)

    def _algorithm(self, baseband, measured_locations):
        """
        Parameters
        ----------
        baseband : numpy.ndarray
            complex baseband signal which has been deskewed

        measured_locations : None
            dummy parameter

        Returns
        -------
        image : SarImage object
            the SAR image
        """
        # array for Stolt interpolated data
        stolt = np.empty_like(baseband)

        # transform data into spatial frequency domain
        spatialfreq = np.fft.fftshift(
                        np.fft.fft(baseband, axis=self.azimuth_axis),
                        axes=self.azimuth_axis)

        # Reference function multiplication
        spatialfreq *= self.reference_function

        # Stolt interpolation
        # Resample signal history which was originally sampled on a uniform
        # Kx-Kr grid to a uniformly sampled Kx-Ky grid
        for i in range(self.Kx.size):
            stolt[i,:] = np.interp(
                            self.Ky_unif,
                            self.Ky[i,:],
                            spatialfreq[i,:],
                            left=np.complex(0.0, 0.0),
                            right=np.complex(0.0, 0.0))

        # apply aperture weighting in range direction
        stolt *= self.range_window[np.newaxis,:]
        # apply aperture weighting in azimuth direction
        stolt *= self.azimuth_window[:,np.newaxis]

        # final image compression
        if self.reference_range == 0:
            image = np.fft.ifft2(stolt)
        else:
            image = np.fft.fftshift(np.fft.ifft2(stolt), axes=self.range_axis)

        return image

    def _compute_variables(self, shape):
        """
        Compute variables needed for the RMA based on the baseband shape

        Parameters
        ----------
        shape : (int, int)
            the shape of the baseband data array

        Sets
        -------
        Kx : np.ndarray
            wavenumber in azimuth direction

        Kr : np.ndarray
            wavenumber in range direction

        Ky : np.ndarray
            non-uniformly spaced Kx-Ky grid

        Ky_unif : np.ndarray
            uniformly spaced Ky sample points used for stolt interpolation

        crossrange : np.ndarray
            image pixel locations in azimuth direction

        downrange : np.ndarray
            image pixel locations in range direction
        """
        Nx,Nr = shape

        # "Doppler factor"
        self.alpha = 1/(1 - (self.acqSettings.platformVelocity/self.acqSettings.c)**2)

        # fast time variable
        self.t = np.linspace(0, self.acqSettings.T, Nr)

        # wavenumber in azimuth (x) direction
        self.Kx = np.pi * np.linspace(-1, 1, Nx) \
                * self.acqSettings.azimuthSampleRate

        # wavenumber in range direction
        self.Kr = 4 * np.pi / self.acqSettings.c \
           * (self.acqSettings.fc + self.acqSettings.s * self.t)

        # wavenumber in y direction, non-uniformly spaced
        Ky = self.Kr[np.newaxis,:]**2 \
           - (self.acqSettings.platformVelocity/self.acqSettings.c*self.Kr[np.newaxis,:] \
           + self.Kx[:,np.newaxis]/self.alpha)**2
        # mask invalid values of Ky
        Ky[Ky < 0.0] = 0.0
        self.Ky = np.sqrt(Ky)

        # Wavenumber in y direction, uniformly spaced
        self.Ky_unif = np.linspace(np.min(self.Ky), np.max(self.Ky), self.Kr.size)

        # crossrange pixel locations in m
        self.crossrange = np.linspace(-int(Nx/2), int(Nx/2)-1, Nx) \
                        / self.acqSettings.azimuthSampleRate

        # downrange pixel locations in m
        self.downrange = np.linspace(-Nr/2, int(Nr/2)-1, Nr) \
                  * 2 * np.pi \
                  / (np.max(self.Ky) - np.min(self.Ky))


        self.reference_function = self._reference_function()
        self.azimuth_window = self._azimuth_window(Nx)
        self.range_window = self._range_window(Nr)

    def _reference_function(self):
        """
        Modified reference function used in RMA
        """
        phi = self.alpha * self.reference_range * self.Ky \
            - self.Kx[:,np.newaxis] * self.acqSettings.platformVelocity \
            * self.t[np.newaxis,:]
        return np.exp(1j*phi)
