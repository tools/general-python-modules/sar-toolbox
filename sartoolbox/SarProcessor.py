import numpy as np
from numpy.polynomial import Polynomial
from scipy import signal,linalg,optimize,interpolate
import logging

from .SarImage import SarImage
from .ImageFormationAlgorithms import RangeMigrationAlgorithm, RangeMigrationAlgorithmMoco, RangeMigrationAlgorithmFMCW, RangeMigrationAlgorithmBistatic

class SarProcessor():

    def __init__(self, acqSettings, procSettings):
        """
        Initialize SAR processor

        Parameters
        ----------
        acqSettings : radarTools.AcquisitionSettings object
            Object that holds the acquisition settings

        procSettings : SarProcessingSettings object
            Object that holds the processing settings
        """
        self.acqSettings = acqSettings
        self.procSettings = procSettings
        self.logger = logging.getLogger(__name__)

        # Initialize variables common to all SAR processing algorithms
        # azimuth axis for np.ndarray
        self.azimuth_axis = 0
        # range axis for np.ndarray
        self.range_axis = 1
        # Doppler factor
        self.alpha = 1/(1-(self.acqSettings.platformVelocity/self.acqSettings.c)**2)
        # reference range = shortest range to scene center
        self.reference_range = linalg.norm(self.acqSettings.trajectoryCenter[1:])

    def compute_image(self, baseband, measured_locations=None):
        """
        Compute an image using the selected algorithm

        Parameters
        ----------
        baseband : np.ndarray
            the baseband signal

        measured_locations : np.ndarray or None
            (Nx,3) array of measured location vectors of the radar platform
            for every range profile in baseband, used for motion compensation

        Returns
        -------
        image : SarImage object
            a complex valued SAR image
        """
        algorithm = self._algorithm()

        preprocessed_baseband = self._preprocess(baseband)

        image = algorithm.compute_image(preprocessed_baseband, measured_locations)

        # perform autofocus if requested
        if self.procSettings.calibration_metric is not None:
            Nx,Nr = image.image.shape
            metric = self._metric()

            rd = np.fft.fft(image.image, axis=self.azimuth_axis)

            initial_guess = np.zeros(self.procSettings.calibration_order-1)
            bounds = optimize.Bounds(-1,1)

            def compensated_image(x):
                p = Polynomial([0,0,*x])
                return np.fft.ifft(rd * np.exp(-1j*p(algorithm.Kx))[:,np.newaxis], axis=self.azimuth_axis)

            result = optimize.minimize(lambda x: metric(compensated_image(x)),
                                       initial_guess,
                                       method='L-BFGS-B',
                                       bounds=bounds,
                                       tol=1e-5)

            calibration_vector = result.x

            image.image = compensated_image(calibration_vector)

        return image

    def _algorithm(self):
        """
        Return an RangeMigrationAlgorithm object according to the processing
        settings
        """
        if self.procSettings.algorithm == "RMA":
            return RangeMigrationAlgorithm(self.acqSettings, self.procSettings)
        elif self.procSettings.algorithm == "RMA-MOCO":
            return RangeMigrationAlgorithmMoco(self.acqSettings,
                                               self.procSettings)
        elif self.procSettings.algorithm == "RMA-FMCW":
            return RangeMigrationAlgorithmFMCW(self.acqSettings,
                                               self.procSettings)
        elif self.procSettings.algorithm == "RMA-BS":
            return RangeMigrationAlgorithmBistatic(self.acqSettings,
                                                   self.procSettings)

    def _deskew_range(self, baseband):
        """
        Perform range deskew filtering on the baseband signal

        Parameters
        ----------
        baseband : np.ndarray
            baseband signal to be deskewed

        Returns
        -------
        bb : np.ndarray
            the deskewed baseband signal

        Notes
        -----
        Because the returns from scatterers of different range arrive at the
        receive antenna at different times, the scatterers' signal support
        in time is not identical for all scatterers, referred to as range skew.
        This shift in time also introduces a residual video phase (RVP) term in
        the baseband signal wich is quadratic in the distance to the scatterer
        and is an artifact of the dechirp on receive process commonly used in
        FMCW radar sensors.

        The range deskew operation removes the RVP by applying a
        filter with frequency-dependent delay to the signal and therefore
        aligns the returns from all scatterers in time.
        """
        Na,Nr = baseband.shape
        bb = np.fft.fft(baseband, axis=self.range_axis)
        freq = np.fft.fftfreq(Nr, Nr/self.acqSettings.T)
        phi = np.pi/self.acqSettings.s*freq**2
        bb *= np.exp(-1j*phi)[np.newaxis,:]
        bb = np.fft.ifft(bb, axis=self.range_axis)
        return bb

    def _analytic_signal(self, x):
        """
        Convert the real valued signal to an analytic signal
        (complex valued representation) such that signal = Re(out)

        This function uses the FFT and IFFT to perform a Hilbert transform of
        the signal and also decimates the sample rate by a factor of 2

        Parameters
        ----------
        x : np.ndarray
            the real-valued baseband signal to be converted

        Returns
        -------
        out : np.ndarray
            the complex-valued baseband signal
        """
        ft = np.fft.fft(x, axis=self.range_axis)[:,int(x.shape[self.range_axis]/2):]
        ft[:,0] *= 0.5
        out = np.fft.ifft(ft, axis=self.range_axis)
        #out = np.conj(signal.decimate(signal.hilbert(x, axis=self.range_axis), 2, axis=self.range_axis))
        return out

    def _preprocess(self, baseband):
        """
        Perform signal preprocessing in preparation of SAR imaging algorithm

        Parameters
        ----------
        baseband : np.ndarray
            the real-valued baseband signals from the FMCW radar sensor

        Returns
        -------
        out : np.ndarray
            the preprocessed baseband data
        """
        analytic_signal = self._analytic_signal(baseband)
        #deskewed_signal = self._deskew_range(analytic_signal)
        #return deskewed_signal
        return analytic_signal

    def _entropy(self,image):
        magsq = np.abs(image)**2
        energy = np.sum(magsq)
        entropy = np.log(energy) - np.sum(magsq * np.log(magsq))/energy
        return entropy

    def _variance(self,image):
        mag = np.abs(image)
        energy = np.sum(mag**2)
        mean = np.mean(mag)
        variance = np.sum((mag - mean)**2)/energy
        return -variance

    def _weighted_variance(self,image):
        mag = np.abs(image)
        energy_g = np.sum(mag**2, axis=0)
        mean_g = np.mean(energy_g)
        energy = np.sum(energy_g)
        mean = np.mean(mag)
        variance = np.sum((energy_g/mean_g*(mag - mean))**2)/energy
        return -variance

    def _sharpness(self, image):
        return -np.sum(np.abs(image)**4)

    def _metric(self):
        if self.procSettings.calibration_metric == "Entropy":
            return self._entropy
        elif self.procSettings.calibration_metric == "Variance":
            return self._variance
        elif self.procSettings.calibration_metric == "Weighted Variance":
            return self._weighted_variance
        elif self.procSettings.calibration_metric == "Sharpness":
            return self._sharpness
