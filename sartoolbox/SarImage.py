import numpy as np

class SarImage():
    """
    A SAR image

    This class allows metadata to be attached to a SAR image
    """
    def __init__(self, image, crossrange, downrange):
        self.image = image
        self.crossrange = crossrange
        self.downrange = downrange

    @property
    def mag(self):
        return np.abs(self.image)

    @property
    def magdB(self):
        return 20*np.log10(self.mag)

    @property
    def phase(self):
        return np.angle(self.image)
