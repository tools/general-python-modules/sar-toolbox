from typing import Iterable

class SarProcessingSettings():
    """ Processing settings object holds information such as window types
    used for aperture weighting during image processing
    """
    def __init__(self,
                 algorithm="RMA",
                 azimuth_window_type="Rectangular",
                 range_window_type="Rectangular",
                 resample_azimuth=False,
                 calibration_metric=None,
                 calibration_order=2,
                 zero_padding=None):
        """
        Parameters
        ----------
        algorithm : str
            Algorithm to be used for image generation

        azimuth_window_type : str
            Window type used for aperture weighting in the azimuth direction
            (default: "Rectangular")

        range_window_type : str
            Window type used for aperture weighting in the range direction
            (default: "Rectangular")

        resample_azimuth : bool (Default: False)
            Interpolate the signal history in azimuth direction if platform
            location data is available

        calibration_metric : str or None (Default: None)
            Use automatic phase calibration to improve image contrast with
            the given metric

        calibration_order : int (Default: 2)
            The order of the polynomial to use for autofocus processing

        zero_padding : int or None
            Apply zero padding in the azimuth direction such that the azimuth
            axis has zero_padding samples (for faster FFT calculation)
        """

        self._azimuth_window_type = None
        self._range_window_type = None
        self._allowed_window_types = None 
        self._available_algorithms = None 
        self._calibration_metric = None 

        self.allowed_window_types = ["Rectangular","Hann","Nuttall","Taylor"]
        self.available_algorithms = ["RMA","RMA-FMCW","RMA-BS","RMA-MOCO"]
        self.available_calibration_metrics = ["Entropy","Variance","Weighted Variance","Sharpness"]

        self.algorithm = algorithm
        self.azimuth_window_type = azimuth_window_type
        self.range_window_type = range_window_type
        self.resample_azimuth = resample_azimuth
        self.calibration_metric = calibration_metric
        self.calibration_order = calibration_order
        self.zero_padding = zero_padding

    @property
    def allowed_window_types(self):
        return self._allowed_window_types
    
    @allowed_window_types.setter
    def allowed_window_types(self,new):
        if isinstance(new, Iterable):
            self._allowed_window_types = new

    @property
    def available_algorithms(self):
        return self._available_algorithms

    @available_algorithms.setter
    def available_algorithms(self, new):
        if isinstance(new, Iterable):
            self._available_algorithms = new

    @property
    def available_calibration_metrics(self):
        return self._available_calibration_metrics

    @available_calibration_metrics.setter
    def available_calibration_metrics(self, new):
        if isinstance(new, Iterable):
            self._available_calibration_metrics = new

    @property
    def algorithm(self):
        return self._algorithm

    @algorithm.setter
    def algorithm(self, new):
        if new in self.available_algorithms:
            self._algorithm = new

    @property
    def calibration_metric(self):
        return self._calibration_metric

    @calibration_metric.setter
    def calibration_metric(self, new):
        if new in self.available_calibration_metrics:
            self._calibration_metric = new

    @property
    def azimuth_window_type(self):
        return self._azimuth_window_type

    @azimuth_window_type.setter
    def azimuth_window_type(self, new):
        if new in self.allowed_window_types:
            self._azimuth_window_type = new

    @property
    def range_window_type(self):
        return self._range_window_type

    @range_window_type.setter
    def range_window_type(self, new):
        if new in self.allowed_window_types:
            self._range_window_type = new
