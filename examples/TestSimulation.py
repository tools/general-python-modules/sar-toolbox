import numpy as np
from matplotlib import pyplot as plt
import sartoolbox as st
import radarTools as rt
import logging

logging.basicConfig(
        level=logging.INFO,
        handlers=[
            logging.StreamHandler()
        ])

logger = logging.getLogger(__name__)

Nx = 256
totalLength = 2 
stepSize = totalLength/Nx

# Setup the acquisition settings
acqSettings = st.SarAcquisitionSettings(
    antennaBeamwidth=30.0,
    trajectoryCenter=np.array([0,10,2]),
    Nx=Nx,
    startStop=True,
    stepSize=stepSize,
    c=299792458,
    M=None,
    fs=2.5e6,
    samplingDelay=0,
    bits=16,
    B=2e9,
    T=2e-3,
    N=None,
    W=None,
    NTx=1,
    NRx=1,
    NTxPerPlatform=1,
    NRxPerPlatform=1,
    fc=8e9)

# processing settings
procSettings = st.SarProcessingSettings(
        algorithm="RMA",
        azimuth_window_type="Rectangular",
        range_window_type="Rectangular",
        zero_padding=1024,
        )

# simulator
simulator = st.SarSimulator(acqSettings)

# processor
processor = st.SarProcessor(acqSettings, procSettings)

# list of scatterers
scats = [
     st.Scatterer(1.0, [0.0,0.0,0.0]),
     st.Scatterer(1.0, [1.0,0.0,0.0]),
     st.Scatterer(1.0, [1.0,1.0,0.0]),
     st.Scatterer(1.0, [1.0,-1.0,0.0]),
     st.Scatterer(1.0, [0.0,1.0,0.0]),
     st.Scatterer(1.0, [0.0,-1.0,0.0]),
     st.Scatterer(1.0, [-1.0,0.0,0.0]),
     st.Scatterer(1.0, [-1.0,1.0,0.0]),
     st.Scatterer(1.0, [-1.0,-1.0,0.0]),
     st.Scatterer(1.0, [2.0,0.0,0.0]),
     st.Scatterer(1.0, [2.0,2.0,0.0]),
     st.Scatterer(1.0, [2.0,-2.0,0.0]),
     st.Scatterer(1.0, [0.0,2.0,0.0]),
     st.Scatterer(1.0, [0.0,-2.0,0.0]),
     st.Scatterer(1.0, [-2.0,0.0,0.0]),
     st.Scatterer(1.0, [-2.0,2.0,0.0]),
     st.Scatterer(1.0, [-2.0,-2.0,0.0])
]

# get simulated radar baseband
baseband = simulator.generate_real_baseband(scats)

# compute radar image
image = processor.compute_image(baseband)

plt.imshow(
    image.mag,
    cmap='inferno',
    extent=(
        np.min(image.downrange),
        np.max(image.downrange),
        np.min(image.crossrange),
        np.max(image.crossrange)
    ))

plt.xlabel("Downrange [m]")
plt.ylabel("Crossrange [m]")

plt.show()
