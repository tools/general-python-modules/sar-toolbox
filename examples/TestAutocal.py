import numpy as np
from numpy.polynomial import Polynomial
from matplotlib import pyplot as plt
import sartoolbox as st
import logging

logging.basicConfig(
        level=logging.INFO,
        handlers=[
            logging.StreamHandler()
        ])

logger = logging.getLogger(__name__)

Nx = 256
totalLength = 5
stepSize = totalLength/Nx

# Setup the acquisition settings
acqSettings = st.SarAcquisitionSettings(
    antennaBeamwidth=30.0,
    trajectoryCenter=np.array([0,10,5]),
    Nx=Nx,
    startStop=True,
    stepSize=stepSize,
    c=299792458,
    M=None,
    fs=1e6,
    samplingDelay=0,
    bits=16,
    B=2e9,
    T=2e-3,
    N=None,
    W=None,
    NTx=1,
    NRx=1,
    NTxPerPlatform=1,
    NRxPerPlatform=1,
    fc=8e9)

# processing settings
procSettings = st.SarProcessingSettings(
        algorithm="RMA",
        azimuth_window_type="Nuttall",
        range_window_type="Nuttall")

# simulator
simulator = st.SarSimulator(acqSettings)

# processor
processor = st.SarProcessor(acqSettings, procSettings)

# list of scatterers
scats = [
     st.Scatterer(1.0, [0.0,0.0,0.0]),
     st.Scatterer(1.0, [1.0,0.0,0.0]),
     st.Scatterer(1.0, [1.0,1.0,0.0]),
     st.Scatterer(1.0, [1.0,-1.0,0.0]),
     st.Scatterer(1.0, [0.0,1.0,0.0]),
     st.Scatterer(1.0, [0.0,-1.0,0.0]),
     st.Scatterer(1.0, [-1.0,0.0,0.0]),
     st.Scatterer(1.0, [-1.0,1.0,0.0]),
     st.Scatterer(1.0, [-1.0,-1.0,0.0]),
     st.Scatterer(1.0, [2.0,0.0,0.0]),
     st.Scatterer(1.0, [2.0,2.0,0.0]),
     st.Scatterer(1.0, [2.0,-2.0,0.0]),
     st.Scatterer(1.0, [0.0,2.0,0.0]),
     st.Scatterer(1.0, [0.0,-2.0,0.0]),
     st.Scatterer(1.0, [-2.0,0.0,0.0]),
     st.Scatterer(1.0, [-2.0,2.0,0.0]),
     st.Scatterer(1.0, [-2.0,-2.0,0.0])
]

# nominal platform locations
platform_locations = acqSettings.platformLocations()

###############################################################################
# Add location errors due to non-ideal flight path
###############################################################################
xe = 10
ye = 0.5/np.sqrt(2)
ze = 0.5/np.sqrt(2)
location_errors = np.zeros((acqSettings.Nx, 3))

# slow time variable
tau = np.linspace(0,1,acqSettings.Nx)*acqSettings.Nx/acqSettings.pulseRepetitionRate
tau2 = 0.5*acqSettings.Nx/acqSettings.pulseRepetitionRate

# constant acceleration
#location_errors[:,0] = xe * tau**2
#location_errors[:,0] = xe * (tau - tau2)**2
#print(location_errors[:,0])
#location_errors[:,0] = np.random.uniform(low=-a,high=a,size=acqSettings.Nx)
#location_errors[:,0] = xe * np.sin(np.linspace(0,np.pi,acqSettings.Nx))
#location_errors[:,1] = np.random.uniform(low=-ye,high=ye,size=acqSettings.Nx)
location_errors[:,1] = ye * np.sin(np.linspace(0,np.pi,acqSettings.Nx))
#location_errors[:,1] = ye * np.sin(np.linspace(0,np.pi/2,acqSettings.Nx))
location_errors[:,2] = ze * np.sin(np.linspace(0,np.pi,acqSettings.Nx))
#location_errors[:,2] = ze * np.sin(np.linspace(0,np.pi/2,acqSettings.Nx))

# assume error is constant for all locations, even in continuous movement mode
platform_locations += location_errors[:,np.newaxis,:]

###############################################################################
# Add trajectory measurement errors
###############################################################################
xme = 0.0
yme = 0.005
zme = 0.1

sigma = 0.01

measurement_errors = np.zeros((acqSettings.Nx,3))

# normally distributed errors in y and z
measurement_errors[:,1:] = np.random.normal(scale=sigma, size=(acqSettings.Nx,2))

#measurement_errors[:,1] = np.random.normal(scale=yme, size=acqSettings.Nx)
#measurement_errors[:,1] = yme*np.linspace(0,1,acqSettings.Nx)
#measurement_errors[:,2] = zme*np.linspace(0,1,acqSettings.Nx)

# simple additive error model for location measurement
measured_locations = platform_locations + measurement_errors[:,np.newaxis,:]

xidx = np.arange(measured_locations.shape[0])
print(measured_locations[:,0,1])
yfit = Polynomial.fit(xidx, measured_locations[:,0,1],deg=8)
zfit = Polynomial.fit(xidx, measured_locations[:,0,2], deg=8)

better_locations = measured_locations.copy()
better_locations[:,:,1] = yfit(xidx)[:,np.newaxis]
better_locations[:,:,2] = zfit(xidx)[:,np.newaxis]

print(better_locations[:,0,1])

plt.plot(xidx,platform_locations[:,0,2])
plt.plot(xidx,measured_locations[:,0,2])
plt.plot(xidx,better_locations[:,0,2])

# get simulated radar baseband
baseband = simulator.generate_real_baseband(scats,
                                            platform_locations=platform_locations)

# compute radar image
image_nocomp = processor.compute_image(baseband)

procSettings.calibration_metric="Entropy"
#procSettings.calibration_metric="Sharpness"
#procSettings.calibration_detrend=True
procSettings.algorithm="RMA-MOCO"
#procSettings.algorithm="RMA"
#procSettings.resample_azimuth=True
processor = st.SarProcessor(acqSettings, procSettings)
#image_comp = processor.compute_image(baseband, measured_locations[:,0,:])
image_comp = processor.compute_image(baseband, better_locations[:,0,:])
#image_comp = processor.compute_image(baseband, platform_locations[:,0,:])

fig, axes = plt.subplots(2,1, sharex=True, sharey=True)

for (ax, image, title) in zip(axes,[image_nocomp, image_comp],["Uncompensated image", "Compensated image"]):
    ax.imshow(
        image.mag,
        cmap='inferno',
        extent=(
            np.min(image.downrange),
            np.max(image.downrange),
            np.min(image.crossrange),
            np.max(image.crossrange)
        ))
    ax.set_title(title)
    ax.set_xlabel("Downrange [m]")
    ax.set_ylabel("Crossrange [m]")

plt.show()
